#!/bin/bash

javacc -OUTPUT_DIRECTORY=src src/Parser.jj
javac -d dist src/errors/*.java src/utils/*.java src/types/*.java src/nodes/*.java src/*.java 
java -cp dist Parser $1 $2 $3 $4

#COMPILER="Compiler.jj"
#JASMIN_PATH="lib/jasmin-2.4/jasmin.jar"

#if [ "$1" = "$COMPILER" ]; then
 # java -jar $JASMIN_PATH Program.j
  #java Program
  #rm Program.class
#fi
