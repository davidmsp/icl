package errors;

public class TypeError extends Exception{
  private static final long serialVersionUID = 1L;

  private String message;

  public TypeError(String op, String type){
    this.message = String.format("%s : argument(s) is/are not %s", op, type);
  }

  public String getErrorMessage(){
    return this.message;
  }
}
