package nodes;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.ProcessBuilder.Redirect;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;

import types.IType;
import types.TFunc;


public class EnvironmentC {
  
  public static final String JASMIN_PATH = "lib/jasmin-2.4/jasmin.jar";
  public static final String CLASSNAME_FORMAT = "frame_%d";
  public static final String FILENAME_FORMAT = "./dist/%s.j";
  public static final String FRAME_FORMAT = "frame_%d";
  public static final int PROGRAM_OFFSET = 3;

  EnvironmentC father;
  private boolean isFuncEnv;
  private int depth, offset;
  private String className;
  private HashMap<String, Coordinates> vars;
  private HashMap<String, Integer> funcs;
  private Environment<IType> types;
  private File program;
  private PrintStream file;
  private ProcessBuilder b;
  
  public EnvironmentC(){
      this.father = null;
      this.types = new Environment<IType>(null);
      this.depth = 0;
      this.className = String.format(CLASSNAME_FORMAT, this.depth);
      this.init();
      this.program=new File(CodeBlock.OUTPUT_FILE);
  }

  public EnvironmentC(EnvironmentC father){
      this.father = father;
      this.types = new Environment<IType>(this.father.getTypes());
      this.depth = father.depth() + 1;
      this.className = String.format(CLASSNAME_FORMAT, this.depth);
      this.init();
  }

  public EnvironmentC(EnvironmentC father, String funcName) {
      this.father = father;
      this.types = new Environment<>(this.father.getTypes());
      this.depth = father.depth() + 1;
      this.className = funcName;
      this.init();
      this.isFuncEnv=true;
  }

  public Environment<IType> getTypes() {
    return this.types;
  }

  private void init(){
    try{
      this.isFuncEnv = false;
      this.offset = 0;
      this.vars = new HashMap<>();
      this.funcs = new HashMap<>();
      this.file = new PrintStream(
        new File(
          String.format(FILENAME_FORMAT, this.className)
        )
      );
      this.b = new ProcessBuilder("java", "-jar", JASMIN_PATH
                                , String.format(FILENAME_FORMAT, this.className)
                                , "-d", "dist");
      this.b.redirectErrorStream(true);
      this.b.redirectOutput(Redirect.INHERIT);
    }catch(Exception e){
      System.err.println(e.getMessage());
    }
  }

  public static void initFrame(String frame, EnvironmentC env, CodeBlock code){
    code.push(String.format("new %s", frame));
    code.push("dup");
    code.push(String.format("invokespecial %s/<init>()V", frame));
    code.push("dup");
    code.push("aload_3");
    code.push(String.format("putfield %s/sl L%s;", frame, env.slType()));
    code.push("astore_3");
  }

  public EnvironmentC beginScope(){
    return new EnvironmentC(this);
  }

  public EnvironmentC endScope(){
    if(this.depth == 0)
      this.globalCompile();
    else
      this.compile();
    return this.father;
  }

  public String getClassName() {
    return this.className;
  }

  public EnvironmentC getTopEnv() {
    EnvironmentC env = this;

    while(env.father != null)
      env = env.father;
    
    return env;
  }

  public int depth(){
    return this.depth;
  }

  public int offset(){
    return this.offset;
  }

  public Coordinates assoc(String id){
    Coordinates coord = new Coordinates(this.depth, this.offset);
    this.vars.put(id, coord);
    this.offset++;
    return coord;
  }

  public int assocFunc(String id, TFunc type) {
    this.funcs.put(id, this.offset);
    this.types.assoc(id, type);
    this.offset++;
    return this.offset - 1;
  }

  public Coordinates find(String id){
    Coordinates coord = this.vars.get(id);

    return coord == null ? this.father.find(id) : coord;
  }

  public int findFunc(String id) {
    if(this.father == null)
      return this.funcs.get(id);
    else
      return this.getTopEnv().findFunc(id);
  }

  public String slType(){
    if(this.depth == 0 || this.isFuncEnv){
      return "java/lang/Object";
    }else{
      return "frame_" + (this.depth() - 1);
    }
  }

  private void globalCompile() {
    try{
    List<String> lines = Files.readAllLines(this.program.toPath());

    int counter = 0;

    for(String id: this.vars.keySet()) {
      Coordinates coord = this.vars.get(id);
      IType type = this.types.find(id);
      lines.add(
        PROGRAM_OFFSET + counter
      , String.format(".field public static %s %s", coord.getField(), type.toString())
      );

      counter++;
    }

    Files.write(this.program.toPath(), lines, StandardCharsets.UTF_8);
    }catch(IOException e){
      System.err.println(e.getMessage());
    }
  }

  private void compile(){
    this.file.println(String.format(".class public %s", this.className));
    this.file.println(".super java/lang/Object");
    this.file.println(String.format(".field public sl L%s;", this.slType()));

    for(String id: this.vars.keySet()){
      this.file.println(
        String.format(".field public %s %s"
                      , this.vars.get(id).getField()
                      , this.types.find(id))
      );
    }

    //init
    this.file.println(".method public <init>()V");
    this.file.println("aload_0");
    this.file.println("invokenonvirtual java/lang/Object/<init>()V");
    this.file.println("return");

    this.file.print(".end method");

    this.file.close();

    try{
      Process p = this.b.start();
      p.waitFor();
    }catch(Exception e){
      System.out.println("Frame Error:");
      System.out.println(e.getMessage());
    }
  }
}
