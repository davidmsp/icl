package nodes;

public class Coordinates {
  
  private int depth;
  private String field;

  public Coordinates(int depth, int offset){
    this.depth = depth;
    this.field = String.format("v%d", offset);
  }

  public int getDepth(){
    return this.depth;
  }

  public String getField(){
    return this.field;
  }
}
