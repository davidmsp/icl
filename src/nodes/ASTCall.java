package nodes;

import java.util.Iterator;
import java.util.List;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TFunc;

public class ASTCall implements ASTNode {

  public static final int OFFSET = 32;

  private String id;
  private List<ASTNode> args;

  public ASTCall(String id, List<ASTNode> args) {
    this.id = id;
    this.args = args;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType type;
    TFunc funcType = (TFunc) typeEnv.find(this.id);
    Iterator<IType> itera = funcType.getParamTypes().iterator();
    
    for(ASTNode arg: this.args){
      type = arg.typecheck(typeEnv);

      if(type.getClass() != itera.next().getClass())
        throw new TypeError("Function call <funcName>(...)", "same type as function requires");
    }

    return funcType.getFuncType();
  }

  private String generateFunctionCall(int funcNo, TFunc funcType) {
    StringBuilder funcDecl = 
      new StringBuilder(
        String.format("f%d(", funcNo)
      );

    for(IType type: funcType.getParamTypes())
      funcDecl.append(type.toString());

    funcDecl.append(")");
    funcDecl.append(funcType.getFuncType().toString());

    return funcDecl.toString();
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    this.typecheck(e.getTypes());

    int funcNo = e.findFunc(this.id);
    System.out.println("asd");
    TFunc funcType = (TFunc) e.getTypes().find(this.id);

    String funcName = generateFunctionCall(funcNo, funcType);

    for(ASTNode node: this.args){
      node.compile(e, code);
    }

    code.push(String.format("invokestatic Program/%s", funcName));
  }
}
