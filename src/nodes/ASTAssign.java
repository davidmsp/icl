package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TBool;
import types.TInt;
import types.TRef;
import types.VMCell;

public class ASTAssign implements ASTNode {

  private ASTNode init, body;

  public ASTAssign(ASTNode init, ASTNode body) {
    this.init = init;
    this.body = body;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    IValue v1 = this.init.eval(e);
    IValue v2 = this.body.eval(e);

    if (v1 instanceof VMCell) {
      ((VMCell) v1).set(v2);

      return v1;
    }

    throw new TypeError("Assign :=", "reference");
  }

  private void compileInt(EnvironmentC e, CodeBlock code) throws TypeError {
    this.init.compile(e, code);
    code.push("checkcast ref_int");
    this.body.compile(e,code);
    code.push("putfield ref_int/v I");
  }

  private void compileBool(EnvironmentC e, CodeBlock code) throws TypeError {
    this.init.compile(e, code);
    code.push("checkcast ref_bool");
    this.body.compile(e,code);
    code.push("putfield ref_bool/v I");
  }

  private void compileRef(EnvironmentC e, CodeBlock code) throws TypeError {
    this.init.compile(e, code);
    code.push("checkcast ref_class");
    this.body.compile(e,code);
    code.push("putfield ref_class/v Ljava/lang/Object;");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    IType type = this.init.typecheck(e.getTypes());
    this.body.typecheck(e.getTypes());
    type = ((TRef) type).getRefType();

    if(type instanceof TInt)
      this.compileInt(e, code);
    else if(type instanceof TBool)
      this.compileBool(e, code);
    else
      this.compileRef(e, code);
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType t1 = this.init.typecheck(typeEnv);
    IType t2 = this.body.typecheck(typeEnv);

    if(t1 instanceof TRef){
      return new TRef(t2);
    }

    throw new TypeError("Assign :=", "reference");
  }
  
}
