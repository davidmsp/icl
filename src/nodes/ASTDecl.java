package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;

public class ASTDecl implements ASTNode {

  private String id;
  private ASTNode init;
  private IType type;

  public ASTDecl(String id, ASTNode init) {
    this.id = id;
    this.init = init;
  }

  public ASTDecl(String id, ASTNode init, IType type) {
    this.id = id;
    this.init = init;
    this.type = type;
  }

  public String getId() {
    return this.id;
  }

  public IType getType() {
    return this.type;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    return this.init.eval(e);
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError{
    this.typecheck(e.getTypes());
    this.init.compile(e, code);
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType type =  this.init.typecheck(typeEnv);

    if(!type.getClass().equals(this.type.getClass()))
      throw new TypeError("Decl def...in", "same type as declared");

    return type;
  }
}
