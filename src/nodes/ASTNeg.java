package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TInt;
import types.VInt;

public class ASTNeg implements ASTNode {

  ASTNode node;

  public ASTNeg(ASTNode node) {
    this.node = node;
  }

  public IValue eval(Environment<IValue> e) throws TypeError {
    IValue val = this.node.eval(e);

    if (val instanceof VInt) {
      return new VInt(-((VInt) val).getValue());
    }

    throw new TypeError("Neg -", "integer");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError{
    this.typecheck(e.getTypes());

    this.node.compile(e, code);
    code.push("ineg");
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType type = this.node.typecheck(typeEnv);

    if(type instanceof TInt){
      return new TInt();
    }

    throw new TypeError("Neg -", "integer");
  }
}
