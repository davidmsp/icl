package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TInt;
import types.VInt;

public class ASTMod implements ASTNode {

  private ASTNode lhs, rhs;

  public ASTMod(ASTNode lhs, ASTNode rhs) {
    this.lhs = lhs;
    this.rhs = rhs;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    IValue v1 = this.lhs.eval(e);
    IValue v2 = this.rhs.eval(e);

    if (v1 instanceof VInt && v2 instanceof VInt) {
      return new VInt(((VInt) v1).getValue() % ((VInt) v2).getValue());
    }

    throw new TypeError("Mod %", "integer");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    this.typecheck(e.getTypes());

    this.lhs.compile(e, code);
    this.rhs.compile(e, code);
    code.push("irem");
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType t1 = this.lhs.typecheck(typeEnv);
    IType t2 = this.rhs.typecheck(typeEnv);

    if(t1 instanceof TInt && t2 instanceof TInt){
      return new TInt();
    }

    throw new TypeError("Mod %", "integer");
  }
  
}
