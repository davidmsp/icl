package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.VBool;

public class ASTWhile implements ASTNode {

  private ASTNode whileExp, doExp;

  public ASTWhile(ASTNode whileExp, ASTNode doExp) {
    this.whileExp = whileExp;
    this.doExp = doExp;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    IValue whileValue = this.whileExp.eval(e);

    if (whileValue instanceof VBool) {
      while (((VBool) whileValue).getValue()) {
        this.doExp.eval(e);
        whileValue = this.whileExp.eval(e);

        if (!(whileValue instanceof VBool))
          break;
        else if (!(((VBool) whileValue).getValue()))
          return null;
      }
    }

    throw new TypeError("While while..do..end", "boolean");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    String label1 = code.nextLabel();
    String label2 = code.nextLabel();

    code.push(String.format("%s:", label1));
    this.whileExp.compile(e, code);
    code.push(String.format("ifeq %s", label2));
    this.doExp.compile(e, code);
    code.push(String.format("goto %s", label1));
    code.push(String.format("%s:", label2));
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    return null;
  }
  
}
