package nodes;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TBool;
import types.TFunc;
import types.TInt;
import types.TRef;
import types.TVoid;
import utils.Param;

public class ASTFunc implements ASTNode {

  private String id;
  private List<Param> params;
  private TFunc type;
  private ASTNode body;

  public ASTFunc(String id, IType type, ASTNode body, List<Param> params) {
    this.id = id;
    this.params = params;
    
    List<IType> paramTypes = new LinkedList<>();
    for(Param param: this.params)
      paramTypes.add(param.getType());

    this.type = new TFunc(paramTypes, type);
    this.body = body;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType type = this.body.typecheck(typeEnv);

    if(type.getClass() == this.type.getFuncType().getClass())
      return this.type.getFuncType();

    throw new TypeError("Function declaration def <funcName>", "same type as function body");
  }

  private String generateFuncDecl(int funcNo, TFunc funcType) {
    StringBuilder funcDecl = 
      new StringBuilder(
        String.format(".method public static f%d(", funcNo)
      );

    for(IType type: funcType.getParamTypes())
      funcDecl.append(type.toString());


    funcDecl.append(")");
    funcDecl.append(funcType.getFuncType().toString());

    return funcDecl.toString();
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {

    // Create func scope frame
    int funcNo = e.assocFunc(this.id, this.type);

    EnvironmentC env = new EnvironmentC(e, String.format("f%d", funcNo));
    Environment<IType> types = env.getTypes();

    for(Param param: this.params){
      env.assoc(param.getId());
      types.assoc(param.getId(), param.getType());
    }

    this.typecheck(env.getTypes());


    // WRITE FUNC TO PROGRAM
    String funcName = String.format("f%d", e.findFunc(this.id));
    TFunc funcType = (TFunc) e.getTypes().find(this.id);

    String funcDecl = this.generateFuncDecl(funcNo, funcType);

    code.append(funcDecl);
    code.append(String.format(".limit locals 10", funcType.getParamTypes().size() + 1));
    code.append(".limit stack 256");
    code.append(String.format("new %s", funcName));
    code.append("dup");
    code.append(String.format("invokespecial %s/<init>()V", funcName));
    code.append("dup");
    code.append("aconst_null");
    code.append(String.format("putfield %s/sl Ljava/lang/Object;", funcName));

    int counter = 0;
    for(Param param: this.params) {
      code.append("dup");

      if(param.getType() instanceof TInt
      ||  param.getType() instanceof TBool)
        code.append(String.format("iload_%d", counter));
      else
        code.append(String.format("aload_%d", counter));

      code.append(String.format("putfield %s/v%d %s"
                          , funcName
                          , counter
                          , param.getType().toString())
      );
      counter++;
    }
    code.append("astore_3");

    // a cheat for the exp compilation below
    CodeBlock block = new CodeBlock();
    this.body.compile(env, block);

    Iterator<String> itera = block.getOps().descendingIterator();

    while(itera.hasNext())
      code.append(itera.next());

    if(this.type.getFuncType() instanceof TRef)
      code.append("areturn");
    else if(this.type.getFuncType() instanceof TVoid)
      code.append("return");
    else
      code.append("ireturn");
      
    code.append(".end method");

    env.endScope();

  }
  
}
