package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TBool;
import types.VBool;

public class ASTNot implements ASTNode {

  private ASTNode node;

  public ASTNot(ASTNode node) {
    this.node = node;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {

    IValue value = this.node.eval(e);

    if (value instanceof VBool) {
      return new VBool(!(((VBool) value).getValue()));
    }

    throw new TypeError("Not ~", "boolean");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    this.typecheck(e.getTypes());

    String label1 = code.nextLabel(), label2 = code.nextLabel();
    
    this.node.compile(e, code);
    code.push("sipush 1");
    code.push("isub");
    code.push(String.format("ifeq %s", label1));
    code.push("sipush 1");
    code.push(String.format("goto %s", label2));
    code.push(String.format("%s:", label1));
    code.push("sipush 0");
    code.push(String.format("%s:", label2));
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType type = this.node.typecheck(typeEnv);

    if(type instanceof TBool) {
      return new TBool();
    }

    throw new TypeError("Not ~", "boolean");
  }
  
}
