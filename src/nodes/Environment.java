package nodes;

import java.util.HashMap;

public class Environment<T> {
  private Environment<T> superScope;
  private HashMap<String, T> vars;

  public Environment(){
    this.superScope = null;
    this.vars = new HashMap<>();
  }

  public Environment(Environment<T> parent){
    this.superScope = parent;
    this.vars = new HashMap<>();
  }

  public Environment<T> beginScope(){
    return new Environment<T>(this);
  }

  public Environment<T> endScope(){
    return this.superScope;
  }

  public void assoc(String id, T val){
    this.vars.put(id, val);
  }

  public T find(String id){
    T val = this.vars.get(id);

    if(val == null)
      val = this.superScope.find(id);

    return val;
  }

  public int size(){
    return this.vars.size();
  }
}
