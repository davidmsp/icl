package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TBool;
import types.VBool;

public class ASTOr implements ASTNode {

  private ASTNode lhs, rhs;

  public ASTOr(ASTNode lhs, ASTNode rhs) {
    this.lhs = lhs;
    this.rhs = rhs;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {

    IValue v1 = this.lhs.eval(e);
    IValue v2 = this.rhs.eval(e);

    if (v1 instanceof VBool && v2 instanceof VBool) {
      return new VBool(((VBool) v1).getValue() || ((VBool) v2).getValue());
    }

    throw new TypeError("Or ||", "boolean");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    this.typecheck(e.getTypes());

    this.lhs.compile(e, code);
    this.rhs.compile(e, code);
    code.push("ior");
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType t1 = this.lhs.typecheck(typeEnv);
    IType t2 = this.rhs.typecheck(typeEnv);

    if(t1 instanceof TBool && t2 instanceof TBool){
      return new TBool();
    }

    throw new TypeError("Or ||", "boolean");
  }
  
}
