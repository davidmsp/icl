package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TBool;
import types.VBool;

public class ASTBool implements ASTNode {

  private boolean value;

  public ASTBool(boolean value) {
    this.value = value;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    return new VBool(this.value);
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) {
    if(this.value)
      code.push("sipush 1");
    else
      code.push("sipush 0");
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    return new TBool();
  }
  
}
