package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;

public class ASTDef implements ASTNode {

  private String id;
  private IType type;
  private ASTNode node;

  public ASTDef(String id, IType type, ASTNode node) {
    this.id = id;
    this.type = type;
    this.node = node;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    // Compiled only
    return null;
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType type = this.node.typecheck(typeEnv);

    if(type.getClass() == this.type.getClass())
      return type;
    throw new TypeError("Def def", "same type as declared");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    this.typecheck(e.getTypes());
    
    //Assumes we are in top level
    CodeBlock global = code.getGlobal();
    this.node.compile(e, global);
    Coordinates coord = e.assoc(this.id);
    e.getTypes().assoc(this.id, this.type);
    global.push(
      String.format("putstatic Program/%s %s"
                  , coord.getField(), type.toString())
    );
  }
  
}
