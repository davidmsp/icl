package nodes;

import java.util.Iterator;
import java.util.List;

import errors.TypeError;
import types.IType;
import types.IValue;

public class ASTDefIn implements ASTNode {

  public static final String FRAME_PATH = "frame_%s";

  private List<ASTDecl> declarations;
  private ASTNode body;

  public ASTDefIn(List<ASTDecl> declarations, ASTNode body) {
    this.declarations = declarations;
    this.body = body;
  }

  public IValue eval(Environment<IValue> e) throws TypeError {
    Iterator<ASTDecl> itera = this.declarations.iterator();

    Environment<IValue> env = e;

    ASTDecl decl = itera.next();
    IValue v1 = decl.eval(env);
    env = env.beginScope();
    env.assoc(decl.getId(), v1);

    while (itera.hasNext()) {
      decl = itera.next();
      v1 = decl.eval(env);
      env.assoc(decl.getId(), v1);
    }

    IValue val = this.body.eval(env);

    return val;
  }

  // COMPILE

  private void compileDecl(ASTDecl decl, String frame, EnvironmentC e, CodeBlock code) throws TypeError{
    decl.typecheck(e.getTypes());

    Coordinates coord = e.assoc(decl.getId());
    Environment<IType> types = e.getTypes();
    IType type = decl.typecheck(types);

    types.assoc(decl.getId(), type);
    code.push("aload_3");
    decl.compile(e, code);
    code.push(String.format("putfield %s/%s %s", frame, coord.getField(), type.toString()));
  }

  private void endLevel(String frame, EnvironmentC env, CodeBlock code) {

    code.push("aload_3");
    code.push(String.format("getfield %s/sl L%s;", frame, env.slType()));
    code.push("astore_3");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    EnvironmentC env;

    env = e.beginScope();
    String frame = String.format(FRAME_PATH, env.depth());
    Iterator<ASTDecl> itera = this.declarations.iterator();

    // environment init
    EnvironmentC.initFrame(frame, env, code);
  
    // declarations init
    while (itera.hasNext()) {
      this.compileDecl(itera.next(), frame, env, code);
    }

    // body eval
    this.body.typecheck(env.getTypes());
    this.body.compile(env, code);
    this.endLevel(frame, env, code);

    env.endScope();
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    for(ASTDecl decl : this.declarations)
      decl.typecheck(typeEnv);

    return this.body.typecheck(typeEnv);
  }
}
