package nodes;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

public class CodeBlock {

  public static final String TEMPLATE_PATH = "./utils/template.j";
  public static final String OUTPUT_FILE = "./Program.j";
  public static final int GLOBAL_OFFSET = 3;
  public static final int OFFSET = 22;

  private LinkedList<String> ops, funcOps;
  private CodeBlock global;

  private File file;

  private int labelCounter;

  
  public CodeBlock(){
    this.ops = new LinkedList<>();
    this.funcOps = new LinkedList<>();
    this.file = new File(TEMPLATE_PATH);
    this.labelCounter = 0;
  }

  public void append(String funcOp) {
    this.funcOps.add(funcOp+ "\n");
  }

  public void push(String op){
    this.ops.push(op);
  }

  public LinkedList<String> getOps() {
    return this.ops;
  }

  public CodeBlock getGlobal() {
    if(this.global == null)
      this.global = new CodeBlock();
    return this.global;
  }

  private void globalCompile() throws IOException {
    List<String> lines = Files.readAllLines(this.file.toPath());
    LinkedList<String> ops = this.global.getOps();

    int numOps = ops.size();

    // init
    lines.add(GLOBAL_OFFSET, ".method static <clinit>()V");

    for(int i = GLOBAL_OFFSET + 1; i < GLOBAL_OFFSET + 1 + numOps; i++){
      lines.add(i, ops.removeLast());
    }

    lines.add(GLOBAL_OFFSET + 1 + numOps, "return");

    lines.add(GLOBAL_OFFSET + 2 + numOps, ".end method");

    Files.write(this.file.toPath(), lines, StandardCharsets.UTF_8);
  }

  private void addPrintOps(boolean printFlag) {
    if(printFlag){
      this.ops.push("invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
      this.ops.push("invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
    }else{
      this.ops.push("pop");
    }
  }

  public void compile(boolean printFlag){
    try{
      List<String> lines = Files.readAllLines(this.file.toPath());

      this.addPrintOps(printFlag);

      int numOps = this.ops.size();

      for(int i = OFFSET; i < OFFSET + numOps; i++){
        lines.add(i, this.ops.removeLast());
      }
      this.file = new File(OUTPUT_FILE);
      this.file.createNewFile();
      Files.write(this.file.toPath(), lines, StandardCharsets.UTF_8);

      if(this.global != null)
        this.globalCompile();

      FileWriter fw = new FileWriter(this.file, true);

      for(String funcOp: this.funcOps)
        fw.append(funcOp);
    
      fw.close();
    }catch(IOException e){
      System.out.println(e.getMessage());
    }
  }

  public String nextLabel() {
    return String.format("L%d", this.labelCounter++);
  }
}
