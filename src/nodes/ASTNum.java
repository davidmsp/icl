package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TInt;
import types.VInt;

public class ASTNum implements ASTNode {

  private int val;

  public ASTNum(int n) {
    val = n;
  }

  public IValue eval(Environment<IValue> e) {
    return new VInt(this.val);
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) {
    code.push("sipush " + this.val);
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    return new TInt();
  }
}

