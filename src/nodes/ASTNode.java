package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;

public interface ASTNode {

    IValue eval(Environment<IValue> e) throws TypeError;

    IType typecheck(Environment<IType> typeEnv) throws TypeError;

    void compile(EnvironmentC e, CodeBlock code) throws TypeError;
}

