package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TBool;
import types.TInt;
import types.TRef;
import types.VMCell;

public class ASTNew implements ASTNode {

  private ASTNode node;

  public ASTNew(ASTNode node) {
    this.node = node;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    return new VMCell(this.node.eval(e));
  }

  private void compileInt(EnvironmentC e, CodeBlock code) throws TypeError {
    code.push("new ref_int");
    code.push("dup");
    code.push("invokespecial ref_int/<init>()V");
    code.push("dup");
    this.node.compile(e, code);
    code.push("putfield ref_int/v I");
  }

  private void compileBool(EnvironmentC e, CodeBlock code) throws TypeError {
    code.push("new ref_bool");
    code.push("dup");
    code.push("invokespecial ref_bool/<init>()V");
    code.push("dup");
    this.node.compile(e, code);
    code.push("putfield ref_bool/v I");
  }

  private void compileRef(EnvironmentC e, CodeBlock code) throws TypeError {
    code.push("new ref_class");
    code.push("dup");
    code.push("invokespecial ref_class/<init>()V");
    code.push("dup");
    this.node.compile(e, code);
    code.push("putfield ref_class/v Ljava/lang/Object;");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    this.typecheck(e.getTypes());

    IType type = this.node.typecheck(e.getTypes());

    if(type instanceof TInt)
      this.compileInt(e, code);
    else if(type instanceof TBool)
      this.compileBool(e, code);
    else
      this.compileRef(e, code);
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    return new TRef(this.node.typecheck(typeEnv));
  }
}
