package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;

public class ASTVar implements ASTNode {

  private String id;

  public ASTVar(String id){
    this.id = id;
  }

  public String getId() {
    return this.id;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    return e.find(this.id);
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) {

    return typeEnv.find(this.id);
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) {
    Coordinates coord = e.find(this.id);
    IType type = e.getTypes().find(id);

    //representes current frame 
    String frame = e.getClassName();

    // if its a global var
    if(coord.getDepth() == 0){
      code.push(
        String.format("getstatic Program/%s %s"
                    , coord.getField(), type.toString())
      );
      return;
    }

    code.push("aload_3");

    //searches for the vars' frame
    for(int i = e.depth(); i > coord.getDepth(); i--){
      code.push(
        String.format(
          "getfield %s/sl L%s;"
        , frame
        , (frame = String.format(EnvironmentC.FRAME_FORMAT, i - 1))
        )
      );
    }

    //pushes the var's value onto the stack
    code.push(
      String.format(
        "getfield %s/%s %s"
      , frame
      , coord.getField()
      , type.toString()
      )
    );
  }
}
