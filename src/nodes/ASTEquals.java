package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.VBool;
import types.VInt;

public class ASTEquals implements ASTNode {

  private ASTNode lhs, rhs;

  public ASTEquals(ASTNode lhs, ASTNode rhs) {
    this.lhs = lhs;
    this.rhs = rhs;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    IValue val1 = this.lhs.eval(e);
    IValue val2 = this.rhs.eval(e);

    if (val1 instanceof VInt && val2 instanceof VInt) {
      return new VBool(((VInt) val1).getValue() == ((VInt) val2).getValue());
    }

    if (val1 instanceof VBool && val2 instanceof VBool) {
      return new VBool(((VBool) val1).getValue() == ((VBool) val2).getValue());
    }

    throw new TypeError("Equals ==", "same type");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    this.typecheck(e.getTypes());

    String label1 = code.nextLabel(), label2 = code.nextLabel();
    
    this.lhs.compile(e, code);
    this.rhs.compile(e, code);
    code.push("isub");
    code.push(String.format("ifne %s", label1));
    code.push("sipush 1");
    code.push(String.format("goto %s", label2));
    code.push(String.format("%s:", label1));
    code.push("sipush 0");
    code.push(String.format("%s:", label2));
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType t1 = this.lhs.typecheck(typeEnv);
    IType t2 = this.rhs.typecheck(typeEnv);

    if(t1.getClass() == t2.getClass()) {
      return t1;
    }

    throw new TypeError("Equals ==", "same type");
  }
  
}
