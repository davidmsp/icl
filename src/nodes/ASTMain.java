package nodes;

import java.util.LinkedList;
import java.util.List;

import errors.TypeError;
import types.IType;
import types.IValue;

public class ASTMain implements ASTNode {

  private List<ASTNode> nodes;
  
  public ASTMain(ASTNode node, ASTMain prevMain) {
    if(prevMain == null)
      this.nodes = new LinkedList<>();
    else
      this.nodes = prevMain.getNodes();

    this.nodes.add(node);
  }

  public List<ASTNode> getNodes() {
    return this.nodes;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    return null;
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    for(ASTNode node: this.nodes)
      node.compile(e, code);
  }
  
}
