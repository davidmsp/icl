package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TBool;
import types.VBool;

public class ASTIf implements ASTNode {

  private ASTNode ifExp, thenExp, elseExp;

  public ASTIf(ASTNode ifExp, ASTNode thenExp, ASTNode elseExp) {
    this.ifExp = ifExp;
    this.thenExp = thenExp;
    this.elseExp = elseExp;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    IValue ifNode = this.ifExp.eval(e);

    if (ifNode instanceof VBool)
      return ((VBool) ifNode).getValue() ? this.thenExp.eval(e) : this.elseExp == null ? null : this.elseExp.eval(e);

    throw new TypeError("Conditional if..then..else..end", "boolean");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    this.typecheck(e.getTypes());

    String label1 = code.nextLabel(), label2 = code.nextLabel();
    
    this.ifExp.compile(e, code);
    code.push(String.format("ifeq %s", label1));
    this.thenExp.compile(e, code);
    code.push(String.format("goto %s", label2));
    code.push(String.format("%s:", label1));

    if(this.elseExp != null)
      this.elseExp.compile(e, code);
    else
      code.push("aconst_null");
    
    code.push(String.format("%s:", label2));
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType ifType = this.ifExp.typecheck(typeEnv);
    IType thenType = this.thenExp.typecheck(typeEnv);
    IType elseType = this.elseExp == null ? null : this.elseExp.typecheck(typeEnv);

    if(ifType instanceof TBool) {
      if(elseType == null)
        return thenType;
      else if(thenType.getClass() == elseType.getClass())
        return thenType;
      else
      throw new TypeError("Conditional if..then..else..end", "same type");
    }

    throw new TypeError("Conditional if..then..else..end", "boolean");
  }
  
}
