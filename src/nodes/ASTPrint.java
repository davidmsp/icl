package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TBool;
import types.TInt;

public class ASTPrint implements ASTNode {

  private ASTNode node;

  public ASTPrint(ASTNode node) {
    this.node = node;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    IValue value = this.node.eval(e);

    System.out.print(value.toString());

    return null;
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    code.push("getstatic java/lang/System/out Ljava/io/PrintStream;");
    this.node.compile(e, code);
    code.push("invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
    code.push("invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V");
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType type = this.node.typecheck(typeEnv);

    if(type instanceof TInt || type instanceof TBool)
      return null;

    throw new TypeError("Print print", "integer or boolean");
  }

}