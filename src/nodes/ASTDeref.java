package nodes;

import errors.TypeError;
import types.IType;
import types.IValue;
import types.TBool;
import types.TInt;
import types.TRef;
import types.VMCell;

public class ASTDeref implements ASTNode {

  private ASTNode node;

  public ASTDeref(ASTNode node) {
    this.node = node;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    IValue val = this.node.eval(e);

    if (val instanceof VMCell) {
      return ((VMCell) val).get();
    }

    throw new TypeError("Deref !", "reference");
  }

  private void compileInt(EnvironmentC e, CodeBlock code) throws TypeError {
    this.node.compile(e, code);
    code.push("checkcast ref_int");
    code.push("getfield ref_int/v I");
  }

  private void compileBool(EnvironmentC e, CodeBlock code) throws TypeError {
    this.node.compile(e, code);
    code.push("checkcast ref_bool");
    code.push("getfield ref_bool/v I");
  }

  private void compileRef(EnvironmentC e, CodeBlock code) throws TypeError {
    this.node.compile(e, code);
    code.push("checkcast ref_class");
    code.push("getfield ref_class/v Ljava/lang/Object;");
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    IType type = this.node.typecheck(e.getTypes());
    type = ((TRef) type).getRefType();

    if(type instanceof TInt)
      this.compileInt(e, code);
    else if(type instanceof TBool)
      this.compileBool(e, code);
    else
      this.compileRef(e, code);
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    IType type = this.node.typecheck(typeEnv);

    if(type instanceof TRef) {
      IType refType = typeEnv.find(((ASTVar) this.node).getId());

      return ((TRef) refType).getRefType();
    }

    throw new TypeError("Deref !", "reference");
  }
}
