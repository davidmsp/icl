package nodes;

import java.util.LinkedList;
import java.util.List;

import errors.TypeError;
import types.IType;
import types.IValue;

public class ASTSeq implements ASTNode {

  private List<ASTNode> nodes;

  public ASTSeq(ASTNode node1, ASTNode node2) {
    this.nodes = new LinkedList<>();

    this.nodes.add(node1);
    this.nodes.add(node2);
  }

  public ASTSeq(ASTSeq nodes, ASTNode newNode) {
    this.nodes = nodes.getNodes();
    this.nodes.add(newNode);
  }

  public List<ASTNode> getNodes() {
    return this.nodes;
  }

  @Override
  public IValue eval(Environment<IValue> e) throws TypeError {
    for (int i = 0; i < this.nodes.size() - 1; i++) {
      this.nodes.get(i).eval(e);
    }

    return this.nodes.get(this.nodes.size() - 1).eval(e);
  }

  @Override
  public void compile(EnvironmentC e, CodeBlock code) throws TypeError {
    this.typecheck(e.getTypes());

    for(int i = 0; i < this.nodes.size() - 1; i++){
      this.nodes.get(i).compile(e, code);
    }

    this.nodes.get(this.nodes.size() - 1).compile(e, code);
  }

  @Override
  public IType typecheck(Environment<IType> typeEnv) throws TypeError {
    return this.nodes.get(this.nodes.size() - 1).typecheck(typeEnv);
  }
}
