package utils;

import types.IType;

public class Param {

  private String id;
  private IType type;

  public Param(String id, IType type) {
    this.id = id;
    this.type = type;
  }

  public String getId() {
    return this.id;
  }

  public IType getType() {
    return this.type;
  }
}
