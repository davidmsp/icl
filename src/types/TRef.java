package types;

public class TRef implements IType {

  private IType type;

  public TRef(IType type) {
    this.type = type;
  }

  public IType getRefType() {
    return this.type;
  }

  @Override
  public String toString(){
    if(type instanceof TInt)
      return "Lref_int;";
    else if(type instanceof TBool)
      return "Lref_bool;";
    else
      return "Lref_class;";
  }
}
