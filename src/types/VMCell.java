package types;

public class VMCell implements IValue{

  private IValue value;

  public VMCell(IValue value){
    this.value = value;
  }

  public IValue get(){
    return this.value;
  }
  
  public void set(IValue value){
    this.value = value;
  }

  @Override
  public String toString(){
    return "REF";
  }

  @Override
  public IType toIType() {
    return new TRef(null);
  }
}
