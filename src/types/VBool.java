package types;

public class VBool implements IValue{

  private boolean value;

  public VBool(boolean value){
    this.value = value;
  }

  public boolean getValue(){
    return this.value;
  }

  @Override
  public String toString(){
    return Boolean.toString(this.value);
  }

  @Override
  public IType toIType() {
    return new TBool();
  }
  
}
