package types;

public class VInt implements IValue {

  private int value;

  public VInt(int value){
    this.value = value;
  }

  public int getValue() {
    return this.value;
  }

  @Override
  public String toString(){
    return Integer.toString(this.value);
  }

  @Override
  public IType toIType() {
    return new TInt();
  }
}
