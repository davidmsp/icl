package types;

public interface IValue {

  String toString();

  IType toIType();

}