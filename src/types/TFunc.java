package types;

import java.util.List;

public class TFunc implements IType {

  private List<IType> paramTypes;
  private IType funcType;

  public TFunc(List<IType> paramTypes, IType funcType) {
    this.paramTypes=paramTypes;
    this.funcType=funcType;
  }

  public List<IType> getParamTypes() {
    return this.paramTypes;
  }

  public IType getFuncType() {
    return this.funcType;
  }
}
