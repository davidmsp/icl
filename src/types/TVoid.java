package types;

public class TVoid implements IType {
  
  public TVoid() {}

  @Override
  public String toString() {
    return "THIS SHOULDNT HAPPEN";
  }
}
