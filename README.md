# ICL
## Single Project Course
## Create a Language Parser
## Instructions
### sudo sh build.sh <Interpreter | Compiler> (Case Insensitive)

## Notes

1.  Took the liberty of changing *def...in* to *let...in* because of conflicts with the global *def...*
2.  Took the liberty of making *println* take an argument instead of just printing a newLine. Now it works like Java's println.
3.  Global vars and funcs do **NOT** work for the interpreter. Please do not call them :D
4.  The project has been done to aim for the **all** the objectives, including the bonus!

## Flags
1. **-f** *FILENAME*: Redirects input to a file
2. **-p** : Prints the return value of the expression. Kind of wonky if used with operations that do not return anything. Might crash.

**IMPORTANT** DO NOT TOUCH UTILS!!!!